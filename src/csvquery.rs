extern crate csv;

use crate::confer;
use crate::filter;
use std::collections::HashMap;
use std::fmt;
use std::io;
use std::num;
use std::process;
use std::str;

#[derive(Debug)]
pub enum CsvQueryError {
    IoError(csv::Error),
    ParseError(String),
}

impl fmt::Display for CsvQueryError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CsvQueryError::IoError(e) => write!(f, "csv io error: {}", e),
            CsvQueryError::ParseError(s) => write!(f, "query parse error: {}", s),
        }
    }
}

impl From<csv::Error> for CsvQueryError {
    fn from(err: csv::Error) -> CsvQueryError {
        CsvQueryError::IoError(err)
    }
}

impl From<filter::Error> for CsvQueryError {
    fn from(err: filter::Error) -> CsvQueryError {
        CsvQueryError::ParseError(err.message)
    }
}

impl From<num::ParseIntError> for CsvQueryError {
    fn from(_err: num::ParseIntError) -> CsvQueryError {
        CsvQueryError::ParseError(String::from("integer parse error"))
    }
}

impl From<str::ParseBoolError> for CsvQueryError {
    fn from(_err: str::ParseBoolError) -> CsvQueryError {
        CsvQueryError::ParseError(String::from("boolean parse error"))
    }
}

pub fn print_head(fname: &str, q: &Query) -> Result<(), CsvQueryError> {
    let mut rdr = csv::ReaderBuilder::new()
        .delimiter(q.delimiter)
        .from_path(fname)?;
    let mut w = csv::WriterBuilder::new()
        .delimiter(q.delimiter)
        .from_writer(io::stdout());

    let mut headers = rdr.headers()?.clone();
    headers.trim();
    w.write_record(&headers)?;

    Ok(())
}

pub fn query_csv(fname: &str, q: &Query) -> Result<(), CsvQueryError> {
    let mut rdr = csv::ReaderBuilder::new()
        .delimiter(q.delimiter)
        .from_path(fname)?;
    let mut w = csv::WriterBuilder::new()
        .delimiter(q.delimiter)
        .from_writer(io::stdout());

    let mut headers = rdr.headers()?.clone();
    headers.trim();

    if q.print_head {
        let map: HashMap<&str, &str> = headers.iter().zip(headers.iter()).collect();
        print_record(&mut w, &map, q, &headers)?;
    }

    let mut limit = q.limit;
    let mut offset = q.offset;
    for result in rdr.records() {
        let mut record = result?;
        record.trim();

        let map: HashMap<&str, &str> = headers.iter().zip(record.iter()).collect();

        if q.filter.match_record(&map, q.strict) {
            if let Some(o) = offset {
                if o > 0 {
                    offset = Some(o - 1);
                    continue;
                }
            }
            if let Some(l) = limit {
                if l == 0 {
                    return Ok(());
                }
                limit = Some(l - 1);
            }
            print_record(&mut w, &map, q, &headers)?;
        }
    }
    Ok(())
}

fn print_record(
    w: &mut csv::Writer<io::Stdout>,
    rec: &HashMap<&str, &str>,
    q: &Query,
    headers: &csv::StringRecord,
) -> Result<(), CsvQueryError> {
    match &q.select {
        QuerySelect::None => return Ok(()),
        QuerySelect::All => {
            let mut row = Vec::new();
            for field in headers {
                match rec.get(field) {
                    Some(s) => row.push(s),
                    None => row.push(&""),
                };
            }
            w.write_record(row)?;
        }
        QuerySelect::Selection(v) => {
            let mut row = Vec::new();
            for field in v {
                match rec.get(field.as_str()) {
                    Some(s) => row.push(s),
                    None => {
                        if q.strict {
                            eprintln!("STRICT MODE! no such field: {}", field);
                            process::exit(10);
                        }
                        row.push(&"");
                    }
                };
            }
            w.write_record(row)?;
        }
    };
    Ok(())
}

#[derive(Debug)]
struct RawQuery {
    mode: QueryMode,
    delimiter: u8,
    strict: bool,
    print_head: bool,
    select: Vec<String>,
    filter: Vec<String>,
    limit: Option<u64>,
    offset: Option<u64>,
}

#[derive(Debug)]
pub struct Query {
    pub mode: QueryMode,
    delimiter: u8,
    strict: bool,
    print_head: bool,
    select: QuerySelect,
    filter: filter::Condition,
    limit: Option<u64>,
    offset: Option<u64>,
}

#[derive(Debug)]
pub enum QueryMode {
    Undefined,
    Select,
    Head,
}

#[derive(Debug)]
enum QuerySelect {
    None,
    All,
    Selection(Vec<String>),
}

#[derive(Debug)]
enum ParserState<'a> {
    Initial,
    Set,
    SetKey(&'a String),
    Select,
    Filter,
    Limit,
    AfterLimit,
    Offset,
    Final,
    UnexpectedToken(&'a String),
}

pub fn parse_query(tokens: &Vec<String>, cfer: &confer::Confer) -> Result<Query, CsvQueryError> {
    let config = cfer.get_config();

    let mut q = RawQuery {
        mode: QueryMode::Undefined,
        delimiter: config.delimiter,
        strict: config.strict,
        print_head: config.print_head,
        select: Vec::new(),
        filter: Vec::new(),
        limit: None,
        offset: None,
    };
    let mut state = ParserState::Initial;
    for token in tokens {
        match state {
            ParserState::Initial => match token.as_str() {
                "HEAD" => {
                    q.mode = QueryMode::Head;
                    state = ParserState::Final;
                }
                "SELECT" => {
                    q.mode = QueryMode::Select;
                    state = ParserState::Select;
                }
                "SET" => state = ParserState::Set,
                _ => state = ParserState::UnexpectedToken(&token),
            },
            ParserState::Set => state = ParserState::SetKey(&token),
            ParserState::SetKey(k) => match k.as_str() {
                "DELIM" => {
                    if !token.is_ascii() || token.len() != 1 {
                        return Err(CsvQueryError::ParseError(String::from(
                            "DELIM must be a single ascii character",
                        )));
                    }
                    q.delimiter = token.bytes().next().unwrap();
                    state = ParserState::Initial;
                }
                "STRICT" => {
                    q.strict = token.parse::<bool>()?;
                    state = ParserState::Initial;
                }
                "PRINT_HEAD" => {
                    q.print_head = token.parse::<bool>()?;
                    state = ParserState::Initial;
                }
                _ => state = ParserState::UnexpectedToken(k),
            },
            ParserState::Select => match token.as_str() {
                "FILTER" => state = ParserState::Filter,
                "LIMIT" => state = ParserState::Limit,
                "OFFSET" => state = ParserState::Offset,
                _ => q.select.push(token.clone()),
            },
            ParserState::Filter => match token.as_str() {
                "LIMIT" => state = ParserState::Limit,
                "OFFSET" => state = ParserState::Offset,
                _ => q.filter.push(token.clone()),
            },
            ParserState::Limit => {
                let limit = token.parse::<u64>()?;
                q.limit = Some(limit);
                state = ParserState::AfterLimit;
            }
            ParserState::AfterLimit => match token.as_str() {
                "OFFSET" => state = ParserState::Offset,
                _ => state = ParserState::UnexpectedToken(&token),
            },
            ParserState::Offset => {
                let offset = token.parse::<u64>()?;
                q.offset = Some(offset);
                state = ParserState::Final;
            }
            ParserState::Final => state = ParserState::UnexpectedToken(&token),
            ParserState::UnexpectedToken(_) => (),
        };
    }

    match state {
        ParserState::Final
        | ParserState::Select
        | ParserState::Filter
        | ParserState::AfterLimit => (),
        ParserState::UnexpectedToken(token) => {
            return Err(CsvQueryError::ParseError(format!(
                "unexpected token {}",
                token
            )))
        }
        _ => return Err(CsvQueryError::ParseError(String::from("invalid query"))),
    };

    let mut query = Query {
        mode: q.mode,
        delimiter: q.delimiter,
        strict: q.strict,
        print_head: q.print_head,
        select: QuerySelect::None,
        filter: filter::Condition::Empty,
        limit: q.limit,
        offset: q.offset,
    };

    match query.mode {
        QueryMode::Select => {
            if q.select.is_empty() {
                return Err(CsvQueryError::ParseError(String::from(
                    "empty SELECT clause",
                )));
            }
            if q.select.len() == 1 && &q.select[0] == "*" {
                query.select = QuerySelect::All;
            } else {
                query.select = QuerySelect::Selection(q.select);
            }

            if !q.filter.is_empty() {
                query.filter = filter::parse_filter(&q.filter)?;
            }

            Ok(query)
        }
        QueryMode::Head => Ok(query),
        QueryMode::Undefined => Err(CsvQueryError::ParseError(String::from("invalid query"))),
    }
}

#[derive(Debug)]
enum TokenizerState {
    Normal,
    Quote,
}

/*
 * 'Lazy quotes' tokenizer
 * Happily accepts quotes in the middle of quoted fields
 */
pub fn tokenize(s: &str) -> Result<Vec<String>, CsvQueryError> {
    let s = s.trim();

    let mut tokens: Vec<String> = Vec::new();

    let mut state = TokenizerState::Normal;
    let mut cont = String::new();
    for token in s.split(" ") {
        if token.is_empty() {
            continue;
        }

        let begin_quote = token.starts_with("\"");
        let end_quote = token.ends_with("\"");
        match state {
            TokenizerState::Normal => {
                if begin_quote && end_quote {
                    tokens.push(token[1..token.len() - 1].to_string());
                } else if begin_quote {
                    cont.push_str(&token[1..]);
                    state = TokenizerState::Quote;
                } else {
                    tokens.push(token.to_string());
                }
            }
            TokenizerState::Quote => {
                if end_quote {
                    let mut s = String::from(" ");
                    s.push_str(&token[0..token.len() - 1]);
                    cont.push_str(&s);
                    tokens.push(cont);
                    cont = String::new();
                    state = TokenizerState::Normal;
                } else {
                    let mut s = String::from(" ");
                    s.push_str(token);

                    cont.push_str(&s);
                }
            }
        };
    }

    match state {
        TokenizerState::Normal => Ok(tokens),
        TokenizerState::Quote => Err(CsvQueryError::ParseError(String::from("unclosed quote"))),
    }
}
