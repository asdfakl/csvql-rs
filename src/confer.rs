use std::env;
use std::fs;
use std::path::PathBuf;
use toml::value::Table;
use toml::Value;

#[derive(Debug)]
pub struct Confer {
    config: Config,
}

#[derive(Debug)]
pub struct Config {
    pub delimiter: u8,
    pub strict: bool,
    pub print_head: bool,
}

impl Confer {
    pub fn get_config(&self) -> &Config {
        &self.config
    }
}

pub fn new() -> Confer {
    if let Some(c) = load_config() {
        return Confer { config: c };
    }

    Confer {
        config: default_config(),
    }
}

fn default_config() -> Config {
    Config {
        delimiter: 44, // ','
        strict: false,
        print_head: true,
    }
}

fn load_config() -> Option<Config> {
    let home = match env::var("HOME") {
        Ok(v) => v,
        Err(_) => return None,
    };

    let mut fname = PathBuf::from(home);
    fname.push(".csvql.toml");

    let rc = match fs::read_to_string(fname) {
        Ok(s) => s,
        Err(_) => return None,
    };

    let tmlv = match rc.parse::<Value>() {
        Ok(v) => v,
        Err(_) => return None,
    };

    match tmlv {
        Value::Table(table) => Some(from_toml_table(table)),
        _ => None,
    }
}

fn from_toml_table(table: Table) -> Config {
    let mut c = default_config();

    // delimiter
    if let Some(v) = table.get("delimiter") {
        if let Value::String(s) = v {
            if s.is_ascii() && s.len() == 1 {
                c.delimiter = s.bytes().next().unwrap();
            }
        }
    }

    // strict
    if let Some(v) = table.get("strict") {
        if let Value::Boolean(b) = v {
            c.strict = *b;
        }
    }

    // print_head
    if let Some(v) = table.get("print_head") {
        if let Value::Boolean(b) = v {
            c.print_head = *b;
        }
    }

    c
}
