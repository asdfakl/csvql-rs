mod confer;
mod csvquery;
mod filter;

use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        usage();
        process::exit(1);
    }

    let cfer = confer::new();

    let tokens = csvquery::tokenize(&args[2]).unwrap_or_else(|e| {
        eprintln!("{}", e);
        process::exit(2);
    });

    let query = csvquery::parse_query(&tokens, &cfer).unwrap_or_else(|e| {
        eprintln!("{}", e);
        process::exit(3);
    });

    match query.mode {
        csvquery::QueryMode::Select => select(&args[1], &query),
        csvquery::QueryMode::Head => head(&args[1], &query),
        csvquery::QueryMode::Undefined => panic!("undefined query mode"),
    };
}

fn usage() {
    eprintln!("Usage: csvql-rs [FILE] [QUERY]");
}

fn select(fname: &str, query: &csvquery::Query) {
    match csvquery::query_csv(fname, query) {
        Ok(_) => process::exit(0),
        Err(e) => {
            eprintln!("{}", e);
            process::exit(4);
        }
    };
}

fn head(fname: &str, query: &csvquery::Query) {
    match csvquery::print_head(fname, query) {
        Ok(_) => process::exit(0),
        Err(e) => {
            eprintln!("{}", e);
            process::exit(5);
        }
    }
}
