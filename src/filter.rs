use std::collections::HashMap;
use std::fmt;
use std::num;
use std::process;

#[derive(Debug)]
pub enum Condition {
    Or(BranchCondition),
    And(BranchCondition),
    Simple(SimpleCondition),
    Empty,
}

#[derive(Debug)]
pub struct BranchCondition {
    branches: Vec<Condition>,
}

pub struct SimpleCondition {
    inverse: bool,
    lhs: String,
    rhs: Rhs,
}

impl fmt::Debug for SimpleCondition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.rhs {
            Rhs::StringEq(s) => write!(f, "{} = {}", self.lhs, s),
            Rhs::StringIneq(s) => write!(f, "{} != {}", self.lhs, s),
            Rhs::FloatLt(fl) => write!(f, "{} < {}", self.lhs, fl),
            Rhs::FloatGt(fl) => write!(f, "{} > {}", self.lhs, fl),
            Rhs::IsInt => write!(f, "{} IS INT", self.lhs),
            Rhs::IsFloat => write!(f, "{} IS FLOAT", self.lhs),
            Rhs::IsEmpty => write!(f, "{} IS EMPTY", self.lhs),
        }
    }
}

#[derive(Debug)]
enum Rhs {
    StringEq(String),
    StringIneq(String),
    FloatLt(f64),
    FloatGt(f64),
    IsInt,
    IsFloat,
    IsEmpty,
}

impl Rhs {
    fn match_lhs(&self, lhs: &String) -> bool {
        match self {
            Rhs::StringEq(s) => lhs == s,
            Rhs::StringIneq(s) => lhs != s,
            Rhs::FloatLt(f) => match lhs.parse::<f64>() {
                Ok(lf) => lf < *f,
                Err(_) => false,
            },
            Rhs::FloatGt(f) => match lhs.parse::<f64>() {
                Ok(lf) => lf > *f,
                Err(_) => false,
            },
            Rhs::IsInt => match lhs.parse::<i64>() {
                Ok(_) => true,
                Err(_) => false,
            },
            Rhs::IsFloat => match lhs.parse::<f64>() {
                Ok(_) => true,
                Err(_) => false,
            },
            Rhs::IsEmpty => lhs.is_empty(),
        }
    }
}

pub struct Error {
    pub message: String,
}

impl From<num::ParseFloatError> for Error {
    fn from(_err: num::ParseFloatError) -> Error {
        Error {
            message: String::from("float parse error"),
        }
    }
}

impl BranchCondition {
    fn push_buf(&mut self, buf: &Vec<String>) -> Result<(), Error> {
        let condition = parse_simple_condition(buf)?;
        let last_index = self.branches.len() - 1;
        let and = self.branches.get_mut(last_index).unwrap();
        and.push_condition(condition);
        Ok(())
    }
}

impl Condition {
    pub fn match_record(&self, rec: &HashMap<&str, &str>, strict: bool) -> bool {
        match self {
            Condition::Or(c) => {
                if c.branches.is_empty() {
                    return true;
                }
                for or in c.branches.iter() {
                    if or.match_record(rec, strict) {
                        return true;
                    }
                }
                false
            }
            Condition::And(c) => {
                if c.branches.is_empty() {
                    return true;
                }
                for and in c.branches.iter() {
                    if !and.match_record(rec, strict) {
                        return false;
                    }
                }
                true
            }
            Condition::Simple(c) => {
                let lhs = match rec.get(c.lhs.as_str()) {
                    Some(s) => s.to_string(),
                    None => {
                        if strict {
                            eprintln!("STRICT MODE! no such field: {}", c.lhs);
                            process::exit(11);
                        }
                        String::from("")
                    }
                };
                if c.inverse {
                    !c.rhs.match_lhs(&lhs)
                } else {
                    c.rhs.match_lhs(&lhs)
                }
            }
            Condition::Empty => true,
        }
    }

    fn push_condition(&mut self, condition: Condition) {
        match self {
            Condition::Or(c) => c.branches.push(condition),
            Condition::And(c) => c.branches.push(condition),
            _ => panic!("Simple and Empty conditions have no child conditions"),
        }
    }
}

pub fn parse_filter(tokens: &Vec<String>) -> Result<Condition, Error> {
    let mut root = BranchCondition {
        branches: vec![Condition::And(BranchCondition {
            branches: Vec::new(),
        })],
    };

    let mut buf: Vec<String> = Vec::new();
    for token in tokens {
        match token.as_str() {
            "AND" => {
                root.push_buf(&buf)?;
                buf.clear();
            }
            "OR" => {
                root.push_buf(&buf)?;
                buf.clear();

                let and = Condition::And(BranchCondition {
                    branches: Vec::new(),
                });
                root.branches.push(and);
            }
            _ => buf.push(token.clone()),
        };
    }
    if !buf.is_empty() {
        root.push_buf(&buf)?;
    }

    Ok(Condition::Or(root))
}

fn parse_simple_condition(buf: &Vec<String>) -> Result<Condition, Error> {
    let mut inverse = false;
    let mut tokens: Vec<String> = Vec::new();
    for token in buf {
        tokens.push(token.clone());
    }

    if tokens.len() > 0 && tokens.get(0).unwrap() == "NOT" {
        inverse = true;
        tokens.remove(0);
    }
    if tokens.len() != 3 {
        return Err(Error {
            message: format!(
                "simple filter condition expects 3 tokens, got {}",
                tokens.len()
            ),
        });
    }

    let lhs = tokens.get(0).unwrap().clone();
    let fns = tokens.get(1).unwrap();
    let rhs = tokens.get(2).unwrap().clone();

    match fns.as_str() {
        "=" => Ok(Condition::Simple(SimpleCondition {
            inverse: inverse,
            lhs: lhs,
            rhs: Rhs::StringEq(rhs),
        })),
        "!=" => Ok(Condition::Simple(SimpleCondition {
            inverse: inverse,
            lhs: lhs,
            rhs: Rhs::StringIneq(rhs),
        })),
        "<" => {
            let f = rhs.parse::<f64>()?;
            Ok(Condition::Simple(SimpleCondition {
                inverse: inverse,
                lhs: lhs,
                rhs: Rhs::FloatLt(f),
            }))
        }
        ">" => {
            let f = rhs.parse::<f64>()?;
            Ok(Condition::Simple(SimpleCondition {
                inverse: inverse,
                lhs: lhs,
                rhs: Rhs::FloatGt(f),
            }))
        }
        "IS" => match rhs.as_str() {
            "INT" => Ok(Condition::Simple(SimpleCondition {
                inverse: inverse,
                lhs: lhs,
                rhs: Rhs::IsInt,
            })),
            "FLOAT" => Ok(Condition::Simple(SimpleCondition {
                inverse: inverse,
                lhs: lhs,
                rhs: Rhs::IsFloat,
            })),
            "EMPTY" => Ok(Condition::Simple(SimpleCondition {
                inverse: inverse,
                lhs: lhs,
                rhs: Rhs::IsEmpty,
            })),
            _ => Err(Error {
                message: format!("unsupported IS pattern: {}", rhs),
            }),
        },
        _ => Err(Error {
            message: String::from("unsupported comparison operator"),
        }),
    }
}
